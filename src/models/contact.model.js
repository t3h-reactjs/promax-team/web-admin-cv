const RefineSearch = {
    id: "",
    name: "",
    email: "",
}

const CreateGetRequest = {
    name: "",
    email: "",
}

const UpdateGetRequest = {
    name: "",
    email: "",
}

const GetResponse = {
    id: "",
    name: "",
    email: "",
}
// user
const ContactRefineSearchUser = {
    id: "",
    firstName: "",
    lastName: "",
    password:"",
}

const ContactCreateGetRequestUser = {
    firstName: "",
    lastName: "",
    password:"",
}

const ContactUpdateGetRequestUser = {
    firstName: "",
    lastName: "",
    password:"",
}

const ContactGetResponseUser = {
    id: "",
    firstName: "",
    lastName: "",
    password:"",
}

export default () => ({
    ContactRefineSearch,
    ContactRefineSearchUser,

    ContactCreateGetRequest,
    ContactCreateGetRequestUser,
    
    ContactUpdateGetRequest,
    ContactUpdateGetRequestUser,

    ContactGetResponse,
    ContactGetResponseUser,
})

// const Model = {
//     RefineSearch: {
//         id: "",
//         name: "",
//         email: "",
//     },

//     CreateGetRequest: {
//         name: '',
//         email: '',
//     },

//     UpdateGetRequest: {
//         name: "",
//         email: "",
//     },

//     GetResponse: {
//         id: "",
//         name: "",
//         email: "",
//     }
// }


