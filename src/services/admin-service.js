import axios from '../configs/axios-api.config';
import PATH_API_CONFIG from '../configs/path-api.config'

let url = PATH_API_CONFIG.CONTACT.URL

const AdminService = {
    getAll: (request) => {
        const response = axios.get(url, request);
        return response;
    },
    getBy: (id) => {
        url += `?id=${id}`;
        const response = axios.get(url);
        return response;
    },
    update: (id, request) => {
        url += `?id=${id}`;
        const response = axios.put(url, request);
        return response;
    },
    create: (request) => {
        const response = axios.post(url, request);
        return response;
    },
    delete: (id) => {
        url += `/${id}`;
        const response = axios.delete(url);
        return response;
    },
    unLock: (id) => {
        url += `?id=${id}&status=${1}`;
        const response = axios.patch(url);
        return response;
    },
    lock: (id) => {
        url += `?id=${id}&status=${0}`;
        const response = axios.patch(url);
        return response;
    },
};

export default AdminService