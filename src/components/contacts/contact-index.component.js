import React, { useState, useEffect } from "react";
import ContactService from "../../services/contact.service";
import ContactRefineSearch from "../../models/contact.model"
import 'bootstrap/dist/css/bootstrap.css';
import { Link } from "react-router-dom";


const ContactIndexComponent = props => {
    const [Models, setTutorials] = useState([]);
    const [currentTutorial, setCurrentTutorial] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    const [searchTitle, setSearchTitle] = useState("");

    useEffect(() => {
        retrieveTutorials();
        setActiveTutorial()
    }, []);

    const retrieveTutorials = () => {
        ContactService.getAll(ContactRefineSearch)
            .then(response => {
                setTutorials(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
    const refreshList = () => {
        retrieveTutorials();
        setCurrentTutorial(null);
        setCurrentIndex(-1);
    };
    const setActiveTutorial = (item, index) => {
        setCurrentTutorial(item);
        setCurrentIndex(index);
    };

    const remove = (id) => {
        ContactService.delete(id)
            .then(response => {
                console.log(response.data);
                alert("Remove successfull")
                window.location.reload()
            })
            .catch(e => {
                alert("Error system")
                console.log(e);
            });
    };
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="form-group align-txt-center">
                        <label className="title">List Products</label>
                        <label className="button-add">
                            <Link to={"/contact/add"}>
                                <button className="btn btn-success width-150px">Add</button>
                            </Link>
                        </label>

                    </div>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                Models ? (
                                    Models.map((item, index) => (
                                        <tr key={index}>
                                            <td>{index}</td>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>
                                                <button className="btn btn-primary">Update</button>
                                                <button className="btn btn-danger margin-l-20px" onClick={() => remove(item.id)}>Delete</button>
                                            </td>
                                        </tr>
                                    ))) : (
                                    <tr>
                                        <td colSpan={4}><label>Not record...</label></td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default ContactIndexComponent;