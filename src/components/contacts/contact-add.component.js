import React, { useState, useEffect } from "react";
import ContactService from "../../services/contact.service";
import contactModel from "../../models/contact.model"
import NOTIFYCATIONS_SYSTEM from "../../configs/notifications.config"
import { Link } from "react-router-dom";
import "./contact.css"

const ContactAddComponent = () => {
    let [request, setRequest] = useState(contactModel.CreateGetRequest);
    let [notify, setNotify] = useState(contactModel.CreateGetRequest);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setRequest({ ...request, [name]: value });
        if (name == "email" && value != "") {
            setNotify({ ...notify, email: "" });
        }
        if (name == "name" && value != "") {
            setNotify({ ...notify, name: "" });
        }
    };

    const saveTutorial = () => {
        if (validInputData(request)) {
            return false;
        }
        ContactService.create(request)
            .then(response => {
                alert(NOTIFYCATIONS_SYSTEM.CONTACT.ADD_SUCCESSFULL)
                window.location.reload()
            })
            .catch(e => {
                alert(NOTIFYCATIONS_SYSTEM.ERROR.SYSTEM)
                console.log(e);
            });
    };

    const validInputData = (input) => {
        console.log(input)
        if (input.email === "") {
            setNotify({ ...notify, email: NOTIFYCATIONS_SYSTEM.CONTACT.NOT_NULL.replace("##", "email") });
            return true;
        }
        if (input.name === "") {
            setNotify({ ...notify, name: NOTIFYCATIONS_SYSTEM.CONTACT.NOT_NULL.replace("##", "name") });
            return true;
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12 align-txt-center">
                    <div className="title">Create new contact</div>
                    <div className="col-md-12 align-txt-left">
                        <label className="col-md-12 pad-left-10">Email: </label>
                        <input type="text" name="email" className="form-control col-md-12" id="email" placeholder="Enter email"
                            value={request.email}
                            onChange={handleInputChange} />
                        <small id="email-small" className="form-text text-danger">{notify.email}</small>
                    </div>
                    <div className="col-md-12 align-txt-left">
                        <label className="col-md-12 pad-left-10">Name: </label>
                        <input type="text" name="name" className="form-control col-md-12" id="name" placeholder="Enter name"
                            value={request.name}
                            onChange={handleInputChange} />
                        <small id="name-small" className="form-text text-danger">{notify.name}</small>
                    </div>
                </div>
            </div>
            <br></br>
            <div className="row">
                <div className="col-md-12">
                    <div className="form-group align-txt-center">
                        <button type="button" className="btn btn-primary width-150px" onClick={saveTutorial}>Add</button>
                        <Link to={"/contact"}>
                            <button className="btn btn-warning width-150px margin-l-20px">Back</button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContactAddComponent;