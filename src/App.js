import logo from './logo.svg';
import { Routes, Route, Link } from "react-router-dom";
import './App.css';
import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import AdminCVIndexComponent from "./component/admin-cv/index"
import AdminCVCreateComponent from "./component/admin-cv/create"

import ContactIndexComponentUser from "./component/admin-user/user-index"

import ContactIndexComponent from "./components/contacts/contact-index.component";

import { LoginPage } from './login/Login';

import ContactAddComponent from "./components/contacts/contact-add.component";


function App() {
  return (
    <div className='App'>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/tutorials" className="navbar-brand">
          T3H- ReactJS
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to="/" className="nav-link">Home</Link>
          </li>
          <li className="nav-item">
            <Link to={"/admin-cv"} className="nav-link">CV</Link>
          </li>
          <li className="nav-item">
            <Link to={"/admin-user"} className="nav-link">User</Link>
          </li>
          <li className="nav-item">
            <Link to={"/contact"} className="nav-link">Contacts</Link>
          </li>
        </div>
      </nav>

      <div className='Apps'>
        <Routes>
          <Route path="*" element={<LoginPage />} />
          
          <Route path="admin-user" element={<ContactIndexComponentUser />} />

          <Route path="admin-cv" element={<AdminCVIndexComponent />} />
          <Route path="admin-cv/add" element={<AdminCVCreateComponent />} />

          <Route path="contact" element={<ContactIndexComponent />} />
          <Route path="contact/add" element={<ContactAddComponent />} />
        </Routes>
      </div>
    </div>
  )
}

export default App;
