import React, { useState, useEffect } from "react";
import AdminUserService from "../../services/admin-user.service";
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css'

const ContactIndexComponentUser = props => {
    const [Models, setusers] = useState([]);
    const [currentTutorialUser, setCurrentTutorialUser] = useState(null);
    const [currentIndexUser, setCurrentIndexUser] = useState(-1);
    const [searchTitleUser, setSearchTitleUser] = useState("");

    useEffect(() => {
        retrieveusers();
        setActiveTutorialUser()
    }, []);

    const retrieveusers = () => {
        AdminUserService.getAll(null)
            .then(response => {
                setusers(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    const refreshListUser = () => {
        retrieveusers();
        setCurrentTutorialUser(null);
        setCurrentIndexUser(-1);
    };

    const setActiveTutorialUser = (item, index) => {
        setCurrentTutorialUser(item);
        setCurrentIndexUser(index);
    };

    const remove = (id) => {
        AdminUserService.delete(id)
            .then(response => {
                console.log(response.data);
                refreshListUser();
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <label className="title">Manage users</label>
                    <label className="button-add">
                        <Link to={"/admin-cv/add"}>
                            <button className="btn btn-success width-150px">Add</button>
                        </Link>
                    </label>

                    <table className="table">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Id</th>
                                <th>FirstName</th>
                                <th>LastName</th>
                                <th>password</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                Models ? (
                                    Models.map((item, index) => (
                                        <tr key={index}>
                                            <td>{index}</td>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>
                                                <button className="btn btn-primary">Update</button>
                                                <button className="btn btn-danger margin-l-20px" onClick={() => remove(item.id)}>Delete</button>
                                            </td>
                                        </tr>
                                    ))) : (
                                    <tr>
                                        <td colSpan={4}><label>Not record...</label></td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};
export default ContactIndexComponentUser;