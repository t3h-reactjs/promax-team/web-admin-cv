import React, { useState, useEffect } from "react";
import AdminService from "../../services/admin-service";
// import ContactCreateGetRequestUser from "../../../models/contact.model"

const ContactAddComponentUser = () => {
  const request = {
    firstName: "Hoang",
    lastName: "Thao Van",
    password: "12345"
  };
  const [submittedUser, setSubmittedUser] = useState(request);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setSubmittedUser({ ...submittedUser, [name]: value });
  };
  const saveTutorial = () => {
    var data = {
      firstName: submittedUser.firstName,
      lastName: submittedUser.lastName,
      password: submittedUser.password,
    };
    AdminService.create(data)
      .then(response => {
        setSubmittedUser({

          firstName: response.data.firstName,
          lastName: response.data.lastName,
          password: response.data.password
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };
  return (
    <div className="submit-form">
      (
      <div>
        {/* first name */}
        <div className="form-group">
          <label htmlFor="title">First Name</label>
          <input
            type="text"
            className="form-control"
            id="firstName"
            required
            value={submittedUser.firstName}
            onChange={handleInputChange}
            name="firstName"
          />
        </div>

        {/* Latst Name */}
        <div className="form-group">
          <label htmlFor="description">Last Name</label>
          <input
            type="text"
            className="form-control"
            id="lastName"
            name="lastName"
            required
            value={submittedUser.lastName}
            onChange={handleInputChange}
          />
        </div>
        {/* Password */}
        <div className="form-group">
          <label htmlFor="description">Password</label>
          <input
            type="text"
            className="form-control"
            id="password"
            name="password"
            required
            value={submittedUser.password}
            onChange={handleInputChange}
          />
        </div>
        <button type="button" onClick={saveTutorial} className="btn btn-success">
          Add
        </button>
      </div>
      )
    </div>
  );
};
export default ContactAddComponentUser;