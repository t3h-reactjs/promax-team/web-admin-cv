import React, { useState, useEffect } from "react";
import AdminService from "../../services/admin-service";
// import ContactRefineSearch from "../../../models/contact.model"
import 'bootstrap/dist/css/bootstrap.css'

const AdminCVIndexComponent = props => {
    const [model, setTutorials] = useState([]);
    const [currentTutorial, setCurrentTutorial] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    const [searchTitle, setSearchTitle] = useState("");

    useEffect(() => {
        retrieveTutorials();
        setActiveTutorial()
    }, []);
    const retrieveTutorials = () => {
        AdminService.getAll(null)
            .then(response => {
                setTutorials(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
    const refreshList = () => {
        retrieveTutorials();
        setCurrentTutorial(null);
        setCurrentIndex(-1);
    };
    const setActiveTutorial = (item, index) => {
        setCurrentTutorial(item);
        setCurrentIndex(index);
    };

    const remove = (id) => {
        console.log(id)
        AdminService.delete(id)
            .then(response => {
                console.log(response.data);
                alert("Remove successfull")
                window.location.reload()
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <label className="title">Manage cv</label>
                    <label className="button-add">
                        <a href='admin-cv/add'>
                            <button className="btn btn-primary width-150px">Add</button>
                        </a>
                    </label>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                model ? (
                                    model.map((item, index) => (
                                        <tr key={index}>
                                            <td>{index}</td>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>
                                                <button className="btn btn-primary">Update</button>
                                                <button className="btn btn-danger margin-l-20px" onClick={() => remove(item.id)}>Delete</button>
                                            </td>
                                        </tr>
                                    ))) : (
                                    <tr>
                                        <td colSpan={4}><label>Not record...</label></td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};
export default AdminCVIndexComponent;