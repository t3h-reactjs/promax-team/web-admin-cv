import React, { useState } from "react";
import AdminService from "../../services/admin-service";
import requestModel from "../../models/admin-cv.model"
import NOTIFYCATIONS_SYSTEM from "../../configs/notifications.config"

const ContactAddComponent = () => {
  const [request, setRequest] = useState(requestModel.CreateGetRequest);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setRequest({ ...request, [name]: value });
  };

  const saveTutorial = () => {
    AdminService.create(request)
      .then(response => {
        alert(NOTIFYCATIONS_SYSTEM.CONTACT.ADD_SUCCESSFULL)
        window.location.reload()
      })
      .catch(e => {
        console.log(e);
      });
  };
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12 align-txt-center">
          <div className="title">Create new contact</div>
          <div className="col-md-12 align-txt-left">
            <label className="col-md-12 pad-left-10">Email: </label>
            <input type="text" name="email" className="form-control col-md-12" id="email" placeholder="Enter email"
              value={request.email}
              onChange={handleInputChange} />
          </div>
          <div className="col-md-12 align-txt-left">
            <label className="col-md-12 pad-left-10">Name: </label>
            <input type="text" name="name" className="form-control col-md-12" id="name" placeholder="Enter name"
              value={request.name}
              onChange={handleInputChange} />
          </div>
        </div>
      </div>
      <br></br>
      <div className="row">
        <div className="col-md-12">
          <div className="form-group align-txt-center">
            <button type="button" className="btn btn-primary width-150px" onClick={saveTutorial}>Add</button>
            <a href="/admin-cv">
              <button className="btn btn-warning width-150px margin-l-20px">Back</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ContactAddComponent;