const PATH_API_CONFIG = {
    CONTACT: {
        URL: "/contact"
    },
    CV: {
        URL: "/cv"
    },
    USER: {
        URL: "/user"
    }
}

export default PATH_API_CONFIG;