const NOTIFYCATIONS_SYSTEM = {
    CONTACT: {
        NOT_NULL: "Request enter ## not null",
        ADD_SUCCESSFULL: "Create record new successfull",
        ADD_ERROR: "Create record new not successful",
        DELETE_SUCCESSFULL: "Delete record successfull",
        DELETE_ERROR: "Delete record not successful"
    },
    ERROR: {
        SYSTEM: "Error system"
    }
}

export default NOTIFYCATIONS_SYSTEM;
